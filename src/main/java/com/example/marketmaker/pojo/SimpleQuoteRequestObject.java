package com.example.marketmaker.pojo;

public class SimpleQuoteRequestObject {

    private int securityId;
    private boolean buy;
    private int quantity;

    /**
     * constructor of simple quote request object
     *
     * @param securityId    security ID as integers
     * @param buy           true is buy and false is sell
     * @param quantity      quantity to buy/sell
     */
    public SimpleQuoteRequestObject(int securityId, boolean buy, int quantity) {
        this.securityId = securityId;
        this.buy = buy;
        this.quantity = quantity;
    }

    public int getSecurityId() {
        return securityId;
    }

    public boolean isBuy() {
        return buy;
    }

    public int getQuantity() {
        return quantity;
    }
}
