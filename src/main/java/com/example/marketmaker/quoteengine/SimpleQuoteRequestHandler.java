package com.example.marketmaker.quoteengine;

import com.example.marketmaker.pojo.SimpleQuoteRequestObject;
import com.example.marketmaker.price.ReferencePriceSource;

/**
 * Asynchronous Handler for Quote Request
 */
public class SimpleQuoteRequestHandler {

    /**
     * handle quote request and send back quote price
     *
     * @param quoteRequestStr   quote request in string format
     */
    public double handleQuoteRequest(String quoteRequestStr, QuoteCalculationEngine simpleQuoteCalculationEngine, ReferencePriceSource simpleReferencePriceSource){

        // Create quote request object using SimpleQuoteRequestObject by parsing quoteRequestStr
        // Factory or Consumer-Producer coding pattern could be used if there are other object types
        SimpleQuoteRequestObject simpleQuoteRequestObject = parseRequestString(quoteRequestStr);

        // Set quote price 0 as invalid price
        double quotePrice = 0;

        // Send back 0 quote price is request is invalid
        if(simpleQuoteRequestObject == null) {
            return quotePrice;
        }

        // Get Reference price for Security ID
        double referencePrice = simpleReferencePriceSource.get(simpleQuoteRequestObject.getSecurityId());

        // Calculate quote price
        quotePrice = simpleQuoteCalculationEngine.calculateQuotePrice(simpleQuoteRequestObject.getSecurityId(), referencePrice,
                simpleQuoteRequestObject.isBuy(), simpleQuoteRequestObject.getQuantity());

        return quotePrice;
    }

    /**
     * parse quote request and store it as an request object
     *
     * @param quoteRequestStr               quote request in string format
     * @return simpleQuoteRequestObject     object storing request parameters
     */
    public SimpleQuoteRequestObject parseRequestString(String quoteRequestStr) {

        // Split request to 3 parameters as securityID, buy and quantity
        String[] quoteRequestParams = quoteRequestStr.trim().split("\\s+");

        // Return null object if the size of parameters is not equal to 3
        if(quoteRequestParams.length != 3 || Integer.parseInt(quoteRequestParams[0])<0 || Integer.parseInt(quoteRequestParams[2])<0 ||
                !(quoteRequestParams[1].equalsIgnoreCase("BUY") || quoteRequestParams[1].equalsIgnoreCase("SELL"))) {
            System.out.println("Invalid Quote Request String:" + quoteRequestStr);
            return null;
        }

        SimpleQuoteRequestObject simpleQuoteRequestObject = null;

        try {
            // Parse parameters into corresponding formats, and create the object
            int securityId = Integer.parseInt(quoteRequestParams[0]);
            boolean buy    = quoteRequestParams[1].equalsIgnoreCase("BUY");
            int quantity   = Integer.parseInt(quoteRequestParams[2]);
            simpleQuoteRequestObject = new SimpleQuoteRequestObject(securityId, buy, quantity);
        }
        catch (Exception e) {
            System.out.println("Convert request parameters fails for request: " + quoteRequestStr);
        }

        return(simpleQuoteRequestObject);
    }
}
