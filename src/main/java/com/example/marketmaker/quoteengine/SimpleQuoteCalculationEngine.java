package com.example.marketmaker.quoteengine;

/**
 * Provides access to to libraries for pricing of quote requests.
 */
public class SimpleQuoteCalculationEngine implements QuoteCalculationEngine {

    /**
     * Calculate the appropriate price for a quote request on a security.
     * This may take a long time to execute.
     *
     * @param securityId     security identifier
     * @param referencePrice reference price to calculate theory price from (e.g. underlying security's price)
     * @param buy            {@code true} if buy, otherwise sell
     * @param quantity       quantity for quote
     * @return calculated quote price
     */
    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity){

        // Set time to calculate quote price needs at most 6 seconds
        int SLEEP_TIME = 6000;

        // Return 0 quote price is reference price is 0
        if(referencePrice == 0){
            return 0;
        }

        // Create random timeout to simulate calculation time
        int randomInt = (int)(Math.random()*SLEEP_TIME);
        try {
            System.out.printf("Thread needs %f seconds to get quote price for security: %d\n", (double)randomInt/1000, securityId);
            Thread.sleep(randomInt);
        } catch(InterruptedException e) {
            System.out.println("Thread Sleep is interrupted");
            e.printStackTrace();
        }

        // Simple implementation to simulate quote price calculation as below
        if(buy) {
            return referencePrice + quantity;
        } else {
            return referencePrice - quantity;
        }
    }

}
