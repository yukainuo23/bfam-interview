package com.example.marketmaker.tcpserver;

import com.example.marketmaker.price.SimpleReferencePriceSource;
import com.example.marketmaker.quoteengine.QuoteCalculationEngine;
import com.example.marketmaker.quoteengine.SimpleQuoteCalculationEngine;
import com.example.marketmaker.quoteengine.SimpleQuoteRequestHandler;
import com.example.marketmaker.price.ReferencePriceSource;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;


/**
 * Create TCP Server to receive request and send response
 * handle quote request and send back quote price
 * Assumption 1: no storage is required for request, simply receive and send back data
 * Assumption 2: one connection for one request only, clients only send other quote request after receiving the quote for the previous data
 * Assumption 3: using ThreadPoolExecutor will create defined number of threads, and throw exception when new requests can't be handled due to
 *               thread pool limitation, in this case more computer resources is needed to load balance the request
 * Assumption 4: quote price is always greater than 0, and 0 quote price means the request can't be handled
 * Assumption 5: if quote price can't be calculated within 5 second, 0 quote price will be returned
 * Assumption 6: Reference Source is updated per request, but it should be updated automatically by listener if the querying takes long time
 */
public class TCPServer {

    /**
     * Main function to run TCP server
     *
     * @param args application arguments
     */
    public static void main(String[] args) throws Exception {

        int PORT_NUM          = 8080;
        int INITIAL_POOL_SIZE = 2;
        int MAX_POOL_SIZE     = 10;
        int KEEP_ALIVE_TIME   = 10;
        int QUEUE_CAPACITY    = 50;

        //Open server socket on port, and will auto close resources when finish
        try (ServerSocket serverSocket = new ServerSocket(PORT_NUM)) {
            // Setup TCP socket
            System.out.println("Start Accepting New Request");

            // Configure ThreadPool
            BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
            ThreadFactory threadFactory = Executors.defaultThreadFactory();

            // Create Thread Pool
            ThreadPoolExecutor executorPool = new ThreadPoolExecutor(INITIAL_POOL_SIZE, MAX_POOL_SIZE,
                    KEEP_ALIVE_TIME, TimeUnit.SECONDS, blockingQueue, threadFactory);

            // Throw exception when new requests can't be handled
            // Additional logic should be implemented to recover and load balance
            executorPool.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());

            // Keep listening to new connection request
            while (true) {
                executorPool.execute(new ConnectionHandler(serverSocket.accept()));
            }

            // Additional logic to be implemented to shutdown application gracefully
        }
    }

    /**
     * Inner static threaded class to handle new connections
     */
    private static class ConnectionHandler implements Runnable {
        // Setup socket for new connection
        private final Socket connection;

        /**
         * Constructor to create serverSocket
         *
         * @param connection new server socket of TCP connection
         */
        ConnectionHandler(Socket connection) {
            this.connection = connection;
        }

        /**
         * Implement run function from runnable interface to handle request
         */
        @Override
        public void run() {
            System.out.println("Connected: " + connection);

            // Setup quote price as o
            double quotePrice;

            // Keep one connection one request
            // If the connection needs to persist, additional logic need to implement
            try (BufferedReader readFromClient = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                 DataOutputStream writeToClient = new DataOutputStream(connection.getOutputStream())){

                // Read request from connection
                String quoteRequest = readFromClient.readLine();
                System.out.println("Received Quote Request: " + quoteRequest);

                // Handle request by selecting request handler, calculation engine, and source
                // Initialize handler and calculation engine here for better testing and compatibility for other types of handler, engine, and source
                SimpleQuoteRequestHandler simpleQuoteRequestHandler = new SimpleQuoteRequestHandler();
                QuoteCalculationEngine simpleQuoteCalculationEngine = new SimpleQuoteCalculationEngine();
                ReferencePriceSource simpleReferencePriceSource = new SimpleReferencePriceSource();
                quotePrice = simpleQuoteRequestHandler.handleQuoteRequest(quoteRequest, simpleQuoteCalculationEngine, simpleReferencePriceSource);

                // Reply back quote price
                writeToClient.writeBytes(quotePrice +"\n");
                System.out.printf("Send back Quote Price '%f' for Request %s\n", quotePrice, quoteRequest);

            } catch (Exception e) {

                System.out.println("Error in handling request: " + connection);
                e.printStackTrace();

            } finally {
                // Shutdown connection
                try {
                    connection.close();
                } catch (IOException e) {
                    System.out.println("Failed to close connection: " + connection);
                }
                System.out.println("Closed: " + connection);
            }
        }
    }
}

