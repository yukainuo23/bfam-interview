package com.example.marketmaker.price;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Implemented Source for reference prices.
 */
public class SimpleReferencePriceSource implements ReferencePriceSource {

    //Use concurrentHashMap to handle Multiple Read and Sync of Data
    public static ConcurrentHashMap<Integer, Double> simpleReferencePriceCache= null;
    public static ReferencePriceSourceListener referencePriceSourceListener = null;

    //Constructor of SimpleReferencePriceSource
    public SimpleReferencePriceSource() {
        if(simpleReferencePriceCache == null) {
            simpleReferencePriceCache = new ConcurrentHashMap<>();
        }
        if(referencePriceSourceListener == null) {
            referencePriceSourceListener = new SimpleReferencePriceSourceListener();
            this.subscribe(referencePriceSourceListener);
        }
    }

    /**
     * Subscribe to changes to reference prices.
     *
     * @param listener callback interface for changes
     */
    @Override
    public void subscribe(ReferencePriceSourceListener listener){
        referencePriceSourceListener = listener;
    }

    /**
     * Get Quote from Security ID
     *
     * @param securityId security Id
     */
    @Override
    public double get(int securityId){
        // Return 0 reference price if referencePriceSourceListener doesn't exist
        if(referencePriceSourceListener == null) {
            System.out.println("No reference price listener found");
            return(0);
        }

        // Update reference price in cache if the securityId exists in cache, otherwise get the reference price from listener and store in cache
        if(simpleReferencePriceCache.containsKey(securityId)) {
            referencePriceSourceListener.referencePriceChanged(securityId, simpleReferencePriceCache.get(securityId));
        } else {
            referencePriceSourceListener.referencePriceChanged(securityId, 0);
        }

        // Get price from updated cache
        double price = simpleReferencePriceCache.get(securityId);
        System.out.printf("Get reference price %f for security %d%n", price, securityId);

        return(price);
    }
}
