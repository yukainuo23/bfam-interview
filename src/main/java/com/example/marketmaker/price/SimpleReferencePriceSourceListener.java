package com.example.marketmaker.price;

/**
 * Callback implementation for {@link ReferencePriceSource}
 */
public class SimpleReferencePriceSourceListener implements ReferencePriceSourceListener {

    /**
     * Called when a price has changed.
     *
     * @param securityId security identifier
     * @param price      reference price
     */
    @Override
    public void referencePriceChanged(int securityId, double price){
        // Check if the price needs to update in cache
        // Simple implementation to return reference price when request to update
        if(SimpleReferencePriceSource.simpleReferencePriceCache.containsKey(securityId)) {
            // Each update the price will add 1
            SimpleReferencePriceSource.simpleReferencePriceCache.put(securityId, price+1);
        }
        else {
            // Default price is the securityId value
            SimpleReferencePriceSource.simpleReferencePriceCache.put(securityId, securityId + 0.0);
        }
    }
}
