package com.example.marketmaker;

import com.example.marketmaker.price.SimpleReferencePriceSource;
import com.example.marketmaker.price.SimpleReferencePriceSourceListener;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;

public class SimpleReferencePriceSourceListenerTest {

    private SimpleReferencePriceSourceListener listener = null;

    @Before
    public void setUp() {
        SimpleReferencePriceSource.simpleReferencePriceCache = new ConcurrentHashMap<>();
        listener = new SimpleReferencePriceSourceListener();
    }

    @After
    public void tearDown() {
        SimpleReferencePriceSource.simpleReferencePriceCache = null;
    }

    @Test
    public void referencePriceChanged() {
        int securityId1 = 123;
        int securityId2 = 234;
        listener.referencePriceChanged(securityId1, 0.0);
        Assert.assertEquals(Double.valueOf(123), SimpleReferencePriceSource.simpleReferencePriceCache.get(securityId1));
        listener.referencePriceChanged(securityId1, 123);
        Assert.assertEquals(Double.valueOf(124), SimpleReferencePriceSource.simpleReferencePriceCache.get(securityId1));
        listener.referencePriceChanged(securityId2, 0.0);
        Assert.assertEquals(Double.valueOf(234), SimpleReferencePriceSource.simpleReferencePriceCache.get(securityId2));
        listener.referencePriceChanged(securityId1, 124);
        Assert.assertEquals(Double.valueOf(125), SimpleReferencePriceSource.simpleReferencePriceCache.get(securityId1));
        listener.referencePriceChanged(securityId2, 234);
        Assert.assertEquals(Double.valueOf(235), SimpleReferencePriceSource.simpleReferencePriceCache.get(securityId2));
    }
}