package com.example.marketmaker;

import com.example.marketmaker.quoteengine.SimpleQuoteRequestHandler;
import com.example.marketmaker.pojo.SimpleQuoteRequestObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Make sure the server is up before run this test
 */
public class TCPServerTest {

    @Test
    public void main() throws Exception{
        String quoteRequestBuy1  = "123 BUY 100";
        String quoteRequestSell1 = "123 SELL 100";
        String quoteRequestBuy2  = "233 BUY 150";
        String quoteRequestSell2 = "233 SELL 50";
        testTCPConnection(quoteRequestBuy1);
        testTCPConnection(quoteRequestSell1);
        testTCPConnection(quoteRequestBuy2);
        testTCPConnection(quoteRequestSell2);
    }

    public void testTCPConnection(String quoteRequest) throws Exception {

        SimpleQuoteRequestHandler simpleQuoteRequestHandler = new SimpleQuoteRequestHandler();
        SimpleQuoteRequestObject simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString(quoteRequest);

        double quotePrice;

        for(int i=0; i<5; i++) {
            //Setup TCP socket
            Socket clientSocket = new Socket("localhost", 8080);

            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            outToServer.writeBytes(quoteRequest + "\n");
            System.out.println("Send New Request: " + quoteRequest);

            quotePrice = Double.parseDouble(inFromServer.readLine());
            System.out.printf("Receive quote price '%s' for request '%s'%n", quotePrice, quoteRequest);

            if(simpleQuoteRequestObject.isBuy()){
                //price increase from 223 to 237
                Assert.assertEquals((Double)quotePrice, Double.valueOf(simpleQuoteRequestObject.getSecurityId()+simpleQuoteRequestObject.getQuantity()+i));
            } else {
                //price decrease from 28 to 33
                Assert.assertEquals((Double)quotePrice, Double.valueOf(simpleQuoteRequestObject.getSecurityId()-simpleQuoteRequestObject.getQuantity()+i+5));
            }

            clientSocket.close();
        }
    }
}