package com.example.marketmaker;

import com.example.marketmaker.price.SimpleReferencePriceSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleReferencePriceSourceTest {

    SimpleReferencePriceSource simpleReferencePriceSource = null;

    @Before
    public void setUp() {
        simpleReferencePriceSource = new SimpleReferencePriceSource();
    }

    @After
    public void tearDown() {
        SimpleReferencePriceSource.simpleReferencePriceCache    = null;
        SimpleReferencePriceSource.referencePriceSourceListener = null;
    }

    @Test
    public void get() {
        int securityId1 = 123;
        int securityId2 = 234;
        Assert.assertEquals(Double.valueOf(123), (Double)simpleReferencePriceSource.get(securityId1));
        Assert.assertEquals(Double.valueOf(124), (Double)simpleReferencePriceSource.get(securityId1));
        Assert.assertEquals(Double.valueOf(234), (Double)simpleReferencePriceSource.get(securityId2));
        Assert.assertEquals(Double.valueOf(125), (Double)simpleReferencePriceSource.get(securityId1));
        Assert.assertEquals(Double.valueOf(235), (Double)simpleReferencePriceSource.get(securityId2));

        SimpleReferencePriceSource.referencePriceSourceListener = null;
        Assert.assertEquals(Double.valueOf(0), (Double)simpleReferencePriceSource.get(securityId1));
    }
}