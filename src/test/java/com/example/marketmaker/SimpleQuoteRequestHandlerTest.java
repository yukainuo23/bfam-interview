package com.example.marketmaker;

import com.example.marketmaker.pojo.SimpleQuoteRequestObject;
import com.example.marketmaker.price.ReferencePriceSource;
import com.example.marketmaker.price.SimpleReferencePriceSource;
import com.example.marketmaker.quoteengine.QuoteCalculationEngine;
import com.example.marketmaker.quoteengine.SimpleQuoteCalculationEngine;
import com.example.marketmaker.quoteengine.SimpleQuoteRequestHandler;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class SimpleQuoteRequestHandlerTest {

    SimpleQuoteRequestHandler simpleQuoteRequestHandler = null;

    @Before
    public void setUp() {
        simpleQuoteRequestHandler = new SimpleQuoteRequestHandler();
    }

    @After
    public void tearDown() {
        simpleQuoteRequestHandler = null;
    }

    @Test
    public void handleQuoteRequest() {
        String validRequestStr   = "123 BUY 234";
        String invalidRequestStr = "12 3 BUY 234";
        int securityId        = 123;
        boolean isBuy         = true;
        int quantity          = 234;
        double referencePrice = 100;
        double quotePrice;

        ReferencePriceSource simpleReferencePriceSource = Mockito.mock(SimpleReferencePriceSource.class);
        when(simpleReferencePriceSource.get(securityId)).thenReturn(referencePrice);

        QuoteCalculationEngine simpleQuoteCalculationEngine = Mockito.mock(SimpleQuoteCalculationEngine.class);
        when(simpleQuoteCalculationEngine.calculateQuotePrice(securityId, referencePrice, isBuy, quantity)).thenReturn(123.0);
        Assert.assertEquals((Double)simpleQuoteCalculationEngine.calculateQuotePrice(securityId, referencePrice, isBuy, quantity), Double.valueOf(123.0));

        quotePrice = simpleQuoteRequestHandler.handleQuoteRequest(validRequestStr, simpleQuoteCalculationEngine, simpleReferencePriceSource);
        Assert.assertEquals((Double)quotePrice, Double.valueOf(123.0));
        quotePrice = simpleQuoteRequestHandler.handleQuoteRequest(invalidRequestStr, simpleQuoteCalculationEngine, simpleReferencePriceSource);
        Assert.assertEquals((Double)quotePrice, Double.valueOf(0));
    }

    @Test
    public void parseRequestString() {
        SimpleQuoteRequestObject simpleQuoteRequestObject;

        simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString("123 BUY 234");
        Assert.assertEquals(simpleQuoteRequestObject.getSecurityId(),123);
        Assert.assertTrue(simpleQuoteRequestObject.isBuy());
        Assert.assertEquals(simpleQuoteRequestObject.getQuantity(),234);

        simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString(" 123  sell  234 ");
        Assert.assertEquals(simpleQuoteRequestObject.getSecurityId(),123);
        Assert.assertFalse(simpleQuoteRequestObject.isBuy());
        Assert.assertEquals(simpleQuoteRequestObject.getQuantity(),234);

        simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString(" 123  asv  234 ");
        Assert.assertNull(simpleQuoteRequestObject);

        simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString(" -123  asv  -234 ");
        Assert.assertNull(simpleQuoteRequestObject);

        simpleQuoteRequestObject = simpleQuoteRequestHandler.parseRequestString(" 12 3  asv  234 ");
        Assert.assertNull(simpleQuoteRequestObject);
    }
}